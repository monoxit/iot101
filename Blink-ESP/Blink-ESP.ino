/*
 * Arduinoに添付のスケッチ例「Blink」をESP8266用にしたもの
 */

// ESP8266のWiFi機能ライブラリを使うことを指定
// ライブラリ：ある機能を簡単に使えるようにするためにあらかじめ作られたプログラム集
#include <ESP8266WiFi.h>

// 電源ON後一度だけ実行される部分。{から}が一度だけ実行される
void setup() {
  WiFi.mode(WIFI_STA);  // WiFiアクセスポイント機能をOFF
  // 13番ピンを出力モードに設定
  pinMode(13, OUTPUT);
}

// 永久に繰り返し実行させる部分
void loop() {
  digitalWrite(13, HIGH);   // LED ON (HIGH は高電圧)
  delay(1000);              // 1000ミリ秒待つ
  digitalWrite(13, LOW);    // LED OFF (LOWは低電圧 ０ボルト）
  delay(1000);              // 1000ミリ秒待つ
}
